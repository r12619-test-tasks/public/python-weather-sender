import requests
from datetime import date, datetime
from config import api_key, url, url_timemachine, units, lang


def get_current_temp(city):
    params={'q': city, 'units': units, 'lang': lang, 'appid': api_key}
    response = requests.get(url, params)
    data = response.json()

    if response.status_code != 200:
        if 'cod' and 'message' in data:
            print(f"Error: {data['cod']}, {city}, {data['message']}")
        return data['cod'], None, None, None, None, None, None

    cod = data['cod']
    lat = data['coord']['lat']
    lon = data['coord']['lon']
    dt = date.fromtimestamp(data['dt'])
    temp = data['main']['temp']
    temp_min = data['main']['temp_min']
    temp_max = data['main']['temp_max']

    return cod, lat, lon, dt, temp, temp_min, temp_max

def get_date_temp(lat, lon, dt):
    dt = int(datetime.combine(dt, datetime.min.time()).timestamp())
    params = {'lat': lat, 'lon': lon, 'dt': dt, 'units': units, 'lang': lang, 'appid': api_key}
    response = requests.get(url_timemachine, params)
    data = response.json()

    if response.status_code != 200:
        if 'cod' and 'message' in data:
            print(f"Error: {data['cod']}, {data['message']}")
        return data['cod'], lat, lon, date.fromtimestamp(dt), None, None, None, None

    timezone_offset = data['timezone_offset']

    t = [d['temp'] for d in data['hourly']]

    h = 12 # полдень

    cod = response.status_code
    lat = data['lat']
    lon = data['lon']
    # dt = date.fromtimestamp(data['current']['dt'])
    dt = date.fromtimestamp(data['hourly'][h]['dt']-timezone_offset)
    # temp = data['current']['temp']
    temp = data['hourly'][h]['temp']
    temp_min = min(t)
    temp_max = max(t)
    temp_avg = round(sum(t)/len(t), 2)

    return cod, lat, lon, dt, temp, temp_min, temp_max, temp_avg


if __name__ == "__main__":
    pass

