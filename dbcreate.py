from dbsql import MsSqlDB


class DBCreate(MsSqlDB):

    def __init__(self, conn_str, reset=False):
        super().__init__(conn_str)
        self.execute_sql('SET DATEFORMAT ymd')
        if reset:
            self.table_city_drop()
            self.table_weather_drop()
        self.table_city_create()
        self.table_weather_create()

    def table_city_drop(self):
        sql = """
              IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[city]') AND type in (N'U'))
              DROP TABLE [dbo].[city]
              """
        self.execute_sql(sql)

    def table_city_create(self):
        sql = """
              IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[city]') AND type in (N'U'))
              CREATE TABLE [dbo].[city](
                  [name] [nvarchar](64) NOT NULL,
                  [able] [bit] NOT NULL,
                  CONSTRAINT [PK_city] PRIMARY KEY CLUSTERED([name] ASC)
                  WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
              ) ON [PRIMARY]
              """
        self.execute_sql(sql)

    def table_weather_drop(self):
        sql = """
              IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[weather]') AND type in (N'U'))
              DROP TABLE [dbo].[weather]
              """
        self.execute_sql(sql)

    def table_weather_create(self):
        sql = """
              IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[weather]') AND type in (N'U'))
              CREATE TABLE [dbo].[weather](
                  [date] [date] NOT NULL,
                  [city] [nvarchar](64) NOT NULL,
                  [temp] [float] NOT NULL,
                  [temp_min] [float] NOT NULL,
                  [temp_max] [float] NOT NULL,
                  CONSTRAINT [PK_weather] PRIMARY KEY CLUSTERED ([date] ASC, [city] ASC)
                  WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
              ) ON [PRIMARY]
              """
        self.execute_sql(sql)


if __name__ == "__main__":
    pass

