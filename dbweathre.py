from dbcreate import DBCreate
from datetime import date, timedelta


class WeatherDB(DBCreate):

    def __init__(self, conn_str, reset=False):
        super().__init__(conn_str, reset)
        self.table_city_insert()

    def table_city_insert(self):
        sql_with = """
              WITH t AS (
                    SELECT 'Москва' [name], 1 [able]
              UNION SELECT 'Санкт-Петербург', 1
              UNION SELECT 'Великий Новгород', 0
              UNION SELECT 'Нижний Новгород', 0
              UNION SELECT 'Воронеж', 1
              UNION SELECT 'Самара', 0
              UNION SELECT 'Астрахань', 0
              UNION SELECT 'Новосибирск', 1
              UNION SELECT 'Архангельск', 0
              UNION SELECT 'Мурманск', 0
              )
              """
        sql_insert = """
              INSERT INTO [dbo].[city]
              SELECT [name], [able]
                FROM t
               WHERE [name] NOT IN (SELECT [name] FROM [dbo].[city])
              """
        sql_update = """
              UPDATE c
                 SET c.[able] = t.[able]
                FROM [dbo].[city] c
                JOIN t ON t.[name] = c.[name]
               WHERE c.[able] <> t.[able]
              """
        sql = sql_with + sql_insert
        self.execute_commit_sql(sql)
        sql = sql_with + sql_update
        self.execute_commit_sql(sql)

    def get_cities(self):
        sql = """
              SELECT [name] FROM [dbo].[city] WHERE [able] = 1 
              """
        return self.execute_sql(sql).to_list()

    def fill_weather(self, days, dt, city, temp, temp_min, temp_max):
        for i in range(days, 0, -1):
            d = dt - timedelta(days=i-1)
            self.update_weather(d, city, temp, temp_min, temp_max)

    def update_weather(self, dt, city, temp, temp_min, temp_max):
        data = self.get_weather(dt, city)
        if data:
            sql = f"""
                  UPDATE [dbo].[weather]
                  SET [temp] = {temp}, [temp_min] = {temp_min}, [temp_max] = {temp_max}
                  WHERE [date] = '{dt}' AND [city] = '{city}'
                    AND ([temp] <> {temp} OR [temp_min] <> {temp_min} OR [temp_max] <> {temp_max})
                  """
            print(f"Update record! {dt}, {city}, {temp}, {temp_min}, {temp_max}")
        else:
            sql = f"""
                  INSERT INTO [dbo].[weather] ([date], [city], [temp], [temp_min], [temp_max])
                  VALUES ('{dt}', '{city}', {temp}, {temp_min}, {temp_max})
                  """
            print(f"Insert record! {dt}, {city}, {temp}, {temp_min}, {temp_max}")
        self.execute_commit_sql(sql)

    def get_weather(self, dt=None, city=None):
        sql = """
              SELECT [date], [city], [temp], [temp_min], [temp_max] FROM [dbo].[weather]
              """
        if dt and city:
            sql_where = f"""
                        WHERE [date] = '{dt}' AND [city] = '{city}'
                        """
            sql += sql_where
        return self.execute_sql(sql).to_dict()

    def get_weather_avg(self, dt=date.today(), days=7):
        sql = f"""
              SELECT w.[city],
                     ROUND(AVG(w.[temp]), 2) AS [temp],
                     ROUND(AVG(w.[temp_min]), 2) AS [temp_min],
                     ROUND(AVG(w.[temp_max]), 2) AS [temp_max]
                FROM [dbo].[weather] w
              --  JOIN [dbo].[city] c ON c.[name] = w.[city]
               WHERE w.[date] BETWEEN '{dt-timedelta(days=days-1)}' AND '{dt}'
              --   AND c.[able] = 1
               GROUP BY w.[city]
              HAVING COUNT(w.[date]) = {days}
              """
        return self.execute_sql(sql).to_dict()


if __name__ == "__main__":
    pass

