import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from config import smtp_server, login, password


def send_email(to=[login], subject='', message=''):
    assert isinstance(to, list)
    if not subject: subject = 'Weather report'
    if not message: message = 'Weather is unavailable now!'

    msg = MIMEMultipart()

    msg['From'] = f'Robot <{login}>'
    msg['To'] = 'Recipient <'+'>, Recipient <'.join(to)+'>'
    msg['Subject'] = subject
    msg.attach(MIMEText(message, 'html'))

    try:
        with smtplib.SMTP(smtp_server) as smtp:
            smtp.starttls()
            smtp.login(login, password)
            smtp.send_message(msg)
        print('Weather report sended!')
    except Exception as e:
        print(f"{e}\nWeather report not sended!")


if __name__ == "__main__":
    pass

